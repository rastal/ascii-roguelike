#include <iostream>
#include <vector>
#include <conio.h>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <time.h>

// Max camera size (works best if these are odd ints).
const int kWidth = 41; 
const int kHeight = 19;
// Player position on camera (always centered).
const int kCameraX = kWidth / 2;
const int kCameraY = kHeight / 2;

// The symbols used for displaying the map and border.
const char kWall = '#';
const char kFloor = '.';
const char kClosedDoor = '+';
const char kOpenDoor = '/';
const std::string kBorder = std::string(kWidth, '-');

// Used for passing coordinates easier as parameters.
struct Position {
    int x;
    int y;
};

// Information for anything that moves around the map.
struct Character {
    std::string name;
    char symbol;
    int x;
    int y;
    bool is_alive;
};

// Used to determine which chunks of the map to display at any given time.
struct Room {
    // Top-left and bottom-right (respectively) bounding coordinates.
    Position position1;
    Position position2;

    bool is_visible;
};

// Used mostly for determining room visibility.
bool operator==(Room room1, Room room2) {
    if (room1.position1.x == room2.position1.x &&
        room1.position1.y == room2.position1.y &&
        room1.position2.x == room2.position2.x &&
        room1.position2.y == room2.position2.y) {
        return true;
    } else {
        return false;
    }
};

struct Door {
    bool is_open;
    int x;
    int y;

    // room_list indices for the rooms this door connects.
    int room1_index;
    int room2_index;
};

struct Player : Character {
    int sanity;     // Not used (yet?), but this seemed like a
                    // better placeholder than "health."
    Room location;
};

char input;
bool running = true;
int narration_count = 0;
std::vector<std::string> map;
std::vector<Character> character_list;
std::vector<Room> room_list;
std::vector<Door> door_list;
std::string narration = "";
Player player;

// Draws the game state onto the terminal using ASCII characters.
void Display() {
    
    std::cout << std::string(15, '\n');
    std::cout << " WASD to move  |  X to quit\n\n";
    std::cout << " Sanity level: " << player.sanity << "\n\n";
    std::cout << narration << "\n";
    std::cout << kBorder << "\n";

    // Accounts for the "camera" shift vs the actual map data.
    int x_shift = player.x - kCameraX;
    int y_shift = player.y - kCameraY;
    // Go through each camera coordinate and draw the character
    // from the appropriate map coordinate.
    for (int i = y_shift; i < y_shift + kHeight; ++i) {
        for (int j = x_shift; j < x_shift + kWidth; ++j) {
            // Display space outside the map as blank.
            if (i < 0 || j < 0 || i >= map.size() || j >= map[i].size()) {
                std::cout << " ";
            }
            // Display all the blank space actually on the map.
            else if (map[i][j] == ' ') {
                std::cout << map[i][j];
            }
            // If the player is on this coordinate, display the player symbol.
            else if (i == player.y && j == player.x) {
                std::cout << player.symbol;
            } else {
                bool drawn = false;
                // Check to see if the current room is visible.
                for (Room room : room_list) {
                    if (j >= room.position1.x && j <= room.position2.x &&
                        i >= room.position1.y && i <= room.position2.y) {
                        if (room.is_visible == true) {
                            // Display the non-player characters and 
                            // regular map if the room is visible 
                            // and hasn't been drawn yet.
                            for (int k = 1; k < character_list.size(); ++k) {
                                Character character = character_list[k];
                                if (i == character.y &&
                                    j == character.x) {
                                    std::cout << character.symbol;
                                } else if (!drawn) {
                                    std::cout << map[i][j];
                                    drawn = true;
                                }
                            }
                        } else if (!drawn) {
                            std::cout << " ";
                            drawn = true;
                        }
                    }
                }
            }
        }
        std::cout << "\n";
    }
    std::cout << kBorder << "\n\n";

    if (narration != "") {
        if (narration_count < 2) {
            ++narration_count;
        } else {
            narration = "";
        }
    } 

    // DEBUG
    //std::cout << room_list[door_list[1].room1_index].is_visible << " ";
    //std::cout << room_list[door_list[1].room2_index].is_visible << " ";
    //std::cout << room_list[1].is_visible << " " << room_list[2].is_visible;
    //std::cout << "\n" << (room_list[door_list[1].room1_index] == player.location);

}

// Opens a door on a given tile and makes the room 
// on the other side of it visible.
void OpenDoor(Character character, Position position) {
    for (int i = 0; i < door_list.size(); ++i) {
        Door door = door_list[i];
        if (door.x == position.x && door.y == position.y) {
            door.is_open = true;
            if (room_list[door.room1_index] == player.location) {
                room_list[door.room2_index].is_visible = true;
            } else {
                room_list[door.room1_index].is_visible = true;
            }
        }
    }
    if (character.name == player.name) {
        narration = "You open the door.";
    } else {
        narration = character.name + " opens the door.";
    }
}

// If a character is on a given tile, returns the character_list index of 
// that character. Otherwise, returns -1.
int CheckForCharacter(Position position) {
    for (int i = 0; i < character_list.size(); ++i) {
        Character character = character_list[i];
        if (position.x == character.x && position.y == character.y) {
            return i;
        }
    }
    return -1;
}

// Called when bumping into a Character who's still alive.
void InteractWithCharacter(Character& character) {
    switch(character.symbol) {
        case 'C': {
            int choice = rand() % 3;
            switch(choice) {
                case 0:
                    narration = "Charles starts shouting like a madman.";
                    player.sanity -= 2;
                    break;
                case 1:
                    narration = "Charles mutters something under his breath.";
                    --player.sanity;
                    break;
                case 2:
                    character.symbol = 'c';
                    character.is_alive = false;
                    narration = "You murdered " + character.name + ".";
                    player.sanity -= 4;
                    break;
            }
            break;
        }
        default:
            break;
    }
}

// Moves the specified character in a particular direction.
// Both player and non-player characters pass in the same
// inputs to determine movement direction.
void Move(Character& character, char input) {
    Position target = { character.x, character.y };
    bool character_there = false;
    
    switch(input) {
        case 'w': {
            --target.y;
            char& target_char = map[target.y][target.x];
            
            int character_index = CheckForCharacter(target);
            if (character_index != -1 && 
                character_list[character_index].is_alive == true) {
                InteractWithCharacter(character_list[character_index]);
            } else if (target_char == kFloor || target_char == kOpenDoor) {
                --character.y;
            } else if (target_char == kClosedDoor) {
                OpenDoor(character, target);
                target_char = kOpenDoor;
            }
            break;
        }
        case 'a': {
            --target.x;
            char& target_char = map[target.y][target.x];
            
            int character_index = CheckForCharacter(target);
            if (character_index != -1 && 
                character_list[character_index].is_alive == true) {
                InteractWithCharacter(character_list[character_index]);
            } else if (target_char == kFloor || target_char == kOpenDoor) {
                --character.x;
            } else if (target_char == kClosedDoor) {
                OpenDoor(character, target);
                target_char = kOpenDoor;
            }
            break;
        }
        case 's': {
            ++target.y;
            char& target_char = map[target.y][target.x];
            
            int character_index = CheckForCharacter(target);
            if (character_index != -1 && 
                character_list[character_index].is_alive == true) {
                InteractWithCharacter(character_list[character_index]);
            } else if (target_char == kFloor || target_char == kOpenDoor) {
                ++character.y;
            } else if (target_char == kClosedDoor) {
                OpenDoor(character, target);
                target_char = kOpenDoor;
            }
            break;
        }
        case 'd': {
            ++target.x;
            char& target_char = map[target.y][target.x];
            
            int character_index = CheckForCharacter(target);
            if (character_index != -1 && 
                character_list[character_index].is_alive == true) {
                InteractWithCharacter(character_list[character_index]);
            } else if (target_char == kFloor || target_char == kOpenDoor) {
                ++character.x;
            } else if (target_char == kClosedDoor) {
                OpenDoor(character, target);
                target_char = kOpenDoor;
            }
            break;
        }
        default:
            break;
    }
}

// Processes player input and responds accordingly.
void Logic() {

    switch(input) {
        case 'x':
            running = false;
            break;
        case 'w':
        case 'a':
        case 's':
        case 'd':
            Move(player, input);
            break;
        default:
            break;
    }

    // Set the current position's room to the player room.
    for (Room& room : room_list) {
        if (room.position1.x < player.x && 
            room.position1.y < player.y &&
            room.position2.x > player.x && 
            room.position2.y > player.y) {
            player.location = room;
        }
    }

    // Randomly move the other characters around.
    for (int i = 1; i < character_list.size(); ++i) {
        int move = rand() % 2;
        if (move == 1 || character_list[i].is_alive == false) {
            break;
        }
        int direction = rand() % 4;
        switch(direction) {
            case 0:
                Move(character_list[i], 'w');
                break;
            case 1:
                Move(character_list[i], 'a');
                break;
            case 2:
                Move(character_list[i], 's');
                break;
            case 3:
                Move(character_list[i], 'd');
                break;
            default:
                break;
        }
    }
}

int main() {

    srand(time(0));

    // Import the map.
    std::string line;
    std::ifstream file_in("level-1-map.txt");
    while (std::getline(file_in, line)) {
        map.push_back(line);
    }

    player.name = "player";
    player.symbol = '@';
    player.x = 2;
    player.y = 1;
    player.sanity = 5;
    character_list.push_back(player);

    Character charles;
    charles.name = "Charles";
    charles.symbol = 'C';
    charles.x = 5;
    charles.y = 15;
    charles.is_alive = true;
    character_list.push_back(charles);

    Room room1 = { 0, 0, 19, 7, true };
    Room room2 = { 7, 7, 9, 13, false };
    Room room3 = { 2, 13, 14, 16, false };
    room_list.push_back(room1);
    room_list.push_back(room2);
    room_list.push_back(room3);

    Door door1 = { false, 8, 7, 0, 1 };
    Door door2 = { false, 8, 13, 1, 2 };
    door_list.push_back(door1);
    door_list.push_back(door2);

    narration = "You awaken in a dank, dark room.";

    while (running) {
        Display();
        input = _getch();
        Logic();
    }

    return 0;
}